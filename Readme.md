##Restful API Currency Conversion
This is an university assignment that I've used BitBucket as version control.

It is a Restful API (web service) for currency conversion as well as a Client component to demonstrate & test the application.

This REST based API (web service) is built using PHP, JavaScript & XML/XPath.

Add this line after the index link:

    ?amnt=10.35&from=GBP&to=JPY&format=xml

Here's the example:

    <conv>
    	<at>2017 Jan 20 00:15</at>
    	<rate>0.810880</rate>
    	<from>
    		<code>GBP</code>
    		<curr>Pound Sterling</curr>
    		<loc>
    		GUERNSEY,ISLE OF MAN,JERSEY,UNITED KINGDOM OF GREAT BRITAIN AND NORTHERN IRELAND (THE)
    		</loc>
    	<amnt>10.35</amnt>
    	</from>
    		<to>
    		<code>JPY</code>
    		<curr>Yen</curr>
    		<loc>JAPAN</loc>
    		<amnt>1,467.16</amnt>
   	 </to>
    </conv>
