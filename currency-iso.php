<?php

    include "config_.php";

    $ISOurl = COUNTRIES_URL;
    $ISOxml = simplexml_load_file($ISOurl) or die("Not Loading");
    
    $xmlFile = new DomDocument("1.0","UTF-8");
    $xmlFile->formatOutput = true;

    $Locations = $xmlFile->createElement("Locations");
    $Locations = $xmlFile->appendChild($Locations);

    foreach ($ISOxml->CcyTbl->CcyNtry as $ISO)
    {
            $ISOcode = $ISO->Ccy;
            $test = "EUR";
                  
            foreach ($ccodes as $arraycode)
            {       
                if (strncasecmp($arraycode, $ISOcode, 3) == 0)
                {   
                    $ISOcode = $ISO->Ccy;
                    $ISOloc = $ISO->CtryNm; 
                    
                    $Location = $xmlFile->createElement("Location");
                    $Locations->appendChild($Location);

                    $code = $xmlFile->createElement("code",$ISOcode);
                    $Location->appendChild($code);

                    $loc = $xmlFile->createElement("loc",$ISOloc);
                    $Location->appendChild($loc);
                }
            }
    }

    echo $string_value = $xmlFile->saveXML();
    $xmlFile->save(COUNTRIES);
?>