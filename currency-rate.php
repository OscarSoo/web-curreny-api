<?php

    $url = RATES_URL;
    $xml = simplexml_load_file($url) or die("Not Loading");
    
    $xmlFile = new DomDocument("1.0","UTF-8");
    $xmlFile->formatOutput = true;
    
    $XMLresources = $xmlFile->createElement("resources");
    $XMLresources = $xmlFile->appendChild($XMLresources);

    foreach ($xml->resources->resource as $resource)
    {
        $code = $resource->field[2];
        
        foreach ($ccodes as $arraycode)
        {
            if(strncasecmp($arraycode,$code, 3) == 0)   
            {
                    //Strval output 2 decimals , substr output select char, (string) cast into String
                    $code = substr($resource->field[2],0,-2);
                    $rate = strval($resource->field[1]);
                    $ts =   (string)$resource->field[3]; 
              
                    $XMLresource = $xmlFile->createElement("resource");
                    $XMLresources->appendChild($XMLresource);

                    $XMLcode = $xmlFile->createElement("code",$code);
                    $XMLresource->appendChild($XMLcode);

                    $XMLrate = $xmlFile->createElement("rate",$rate);
                    $XMLresource->appendChild($XMLrate);

                    $XMLts = $xmlFile->createElement("ts",$ts);
                    $XMLresource->appendChild($XMLts); 
            }
        }
    }
    echo $string_value = $xmlFile->saveXML();
    $xmlFile->save(RATES); 
?>