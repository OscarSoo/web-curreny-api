<?php 

?>

<html>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <head>
    </head>
    <body>
       <center>
        <h1>Currency Conversion Services</h1>
        <div class ="outline">
        <div class="table1">
        <form action="form_request.php" name="currency" method="POST">
            <table>
                <th> Form Interface for POST, PUT & DELETE</th>
                <tr>
                <td>Action</td></tr>
                <tr>                   
                    <td ><input type="radio" name="radio" value="Post">&nbsp;&nbsp;Post&nbsp;&nbsp;
                        <input type ="radio" name ="radio" value="Put">&nbsp;&nbsp;Put&nbsp;&nbsp;
                        <input type="radio" name ="radio" value="Delete">&nbsp;&nbsp;Delete&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                       <td>Currency Code</td>
                </tr>
                   <td><input type="text" class="box" name="currencycode" size="40" style="font-size:12pt" placeholder="code"></td>           
                <tr>
                    <td>Currency Name</td>
                </tr>
                   <td><input type="text" class="box" name="currencyname" size="40" style="font-size:12pt" placeholder="name"></td>    
                <tr>
                    <td>Rate (£=1)</td>
                </tr>
                   <td><input type="text" class="box" name="rate" size="40" style="font-size:12pt" placeholder="rate"></td>    
                <tr>
                    <td>Countries(comma seperated if 1+)</td>
                </tr>
                <tr>
                    <td><input type="text" class="box" name="countries" size="40" style="font-size:12pt" placeholder="countries"></td>  
                </tr>                    
                <tr>           
                   <td> <input type="submit" name="submit" class="btn btn-success"></td>
                </tr>  
        
        </table>
        </form>
        </div>
        <br>
        <div class="table2">
        <form name="responsemsg" method="post">
            <table>
                <tr>
                    <th>Response Message:</th></tr>
                    <td><textarea rows="15" cols="55" style="font-size:12pt" placeholder="Response XML"></textarea></td>               
            </table>
        </form>
        </div>
        </div>
        </center>
    </body>
    <footer>
      <p align="center">Author by: Oscar Soo</p>
    </footer>

</html>