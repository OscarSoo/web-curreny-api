<?php
function XML($ccdoes, $Ratexml, $ISOxml, $inputFrom, $inputTo, $amount) //Run the files
{
    $xmlFile = new DomDocument("1.0","UTF-8");
    $xmlFile->formatOutput = true;
    
    $XMLconv = $xmlFile->createElement("conv");
    $XMLconv = $xmlFile->appendChild($XMLconv);
     
    //Loop Rate.xml for From to get code, time, rate
    foreach ($Ratexml->resource as $resource) 
    {
        $RateCode = $resource->code;
        $RateRate = $resource->rate;
        $RateTs   = intval($resource->ts);
   
        if ($inputFrom == $RateCode) 
        {    
            $FromRate = $RateRate; //for calculation purposes
        
            $XMLat = $xmlFile->createElement("at",date('Y M d H:i',time())); 
            $XMLconv->appendChild($XMLat);
            
            $XMLrate = $xmlFile->createElement("rate",number_format(floatval($RateRate),2));
            $XMLconv->appendChild($XMLrate);
            
            $XMLfrom = $xmlFile->createElement("from");
            $XMLconv->appendChild($XMLfrom);
            
            $XMLcode = $xmlFile->createElement("code",$RateCode);
            $XMLfrom->appendChild($XMLcode);                  
        }
    }
    //Loop ISO.xml for From to get currency full name
    foreach($ISOxml->Location as $location) 
    {
        $ISOcode =  $location->code;
        $ISOcur  =  $location->curr;

        if ($inputFrom == $ISOcode)
        {
            $XMLcurr = $xmlFile->createElement("curr",$ISOcur);
            $XMLfrom->appendChild($XMLcurr);
            break;
        }
    }
    
    //Loop ISO.xml for From locations
    foreach($ISOxml->Location as $location) 
    {
        $ISOcode =  $location->code;
        $ISOloc  =  $location->loc;
        
        if ($inputFrom == $ISOcode)
        {
                $arrayFrom[] = $ISOloc;  //input is stored in array
        }
    }
    $ISOlocFrom = implode(",", $arrayFrom); //combine array into string
    $XMLloc = $xmlFile->createElement("loc",$ISOlocFrom);
    $XMLfrom->appendChild($XMLloc);      

    $XMLloc = $xmlFile->createElement("amnt",$amount);
    $XMLfrom->appendChild($XMLloc);
    
    $XMLto = $xmlFile->createElement("to");
    $XMLconv->appendChild($XMLto);

    //Loop TO
    foreach ($Ratexml->resource as $resource)  //Loop Rate.xml for TO
    {
        $RateCode = $resource->code;
        $RateRate = $resource->rate;
        $RateTs   = intval($resource->ts);
        
        if ($inputTo == $RateCode) 
        {    
                $ToRate = $RateRate;          
                $XMLtocode = $xmlFile->createElement("code",$RateCode);
                $XMLto->appendChild($XMLtocode);
        } 
    }
    
    foreach($ISOxml->Location as $location) //Loop ISO.xml for curr
    {
        $ISOcode =  $location->code;
        $ISOloc  =  $location->loc;
        $ISOcur  =  $location->curr;

        if ($inputTo == $ISOcode)
        {
            $XMLtocurr = $xmlFile->createElement("curr",$ISOcur);
            $XMLto->appendChild($XMLtocurr);
            break;
        }
    }
    
    foreach($ISOxml->Location as $location) //Loop ISO.xml for locations
    {
        $ISOcode =  $location->code;
        $ISOloc  =  $location->loc;
        
        if ($inputTo == $ISOcode)
        {
                $arrayTo[] = $ISOloc;  //input is stored in array
        }
    }
    $ISOlocTo = implode(",", $arrayTo); //combine array into string       
    $XMLtoloc = $xmlFile->createElement("loc",$ISOlocTo);
    $XMLto->appendChild($XMLtoloc);
    
    $result = $amount / floatval($FromRate); //calculation
    $Finalresult = $result * floatval($ToRate); //calculation
    
    $XMLtoloc = $xmlFile->createElement("amnt",number_format($Finalresult,2)); //final calculation printed
    $XMLto->appendChild($XMLtoloc); 
    
    $xml_content = $xmlFile->saveXML($XMLconv);    
    file_put_contents(RESULTxml, $xml_content);
}

function JSON() //Output JSON
{
    $ISOpath = RESULTxml;
    $xml = file_get_contents($ISOpath); 
    $xml = str_replace(array("\n", "\r", "\t"), '', $xml);
    $xml = trim(str_replace('"', "'", $xml));
    $simpleXml = simplexml_load_string("<conv>".$xml."</conv>");
    
    $json = json_encode($simpleXml,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE); // | is used for multiple encode functions for print properly and unicode
    file_put_contents(RESULTjson, $json);
}

function CreateRates($ccodes) //Generate Rates.xml
{     
    $url = RATES_URL;
    $xml = simplexml_load_file($url) or die("Not Loading");
    
    $xmlFile = new DomDocument("1.0","UTF-8");
    $xmlFile->formatOutput = true;

    $XMLresources = $xmlFile->createElement("resources");
    $XMLresources = $xmlFile->appendChild($XMLresources);

    foreach ($xml->resources->resource as $resource)
    {
        $code = $resource->field[2];
        
        foreach ($ccodes as $arraycode)
        {
            if(strncasecmp($arraycode,$code, 3) == 0)   
            {
                    //Strval output 2 decimals , substr output select char, (string) cast into String
                    $code = substr($resource->field[2],0,-2);
                    $rate = strval($resource->field[1]);
                    $ts =   (string)$resource->field[3]; 
              
                    $XMLresource = $xmlFile->createElement("resource");
                    $XMLresources->appendChild($XMLresource);

                    $XMLcode = $xmlFile->createElement("code",$code);
                    $XMLresource->appendChild($XMLcode);

                    $XMLrate = $xmlFile->createElement("rate",$rate);
                    $XMLresource->appendChild($XMLrate);

                    $XMLts = $xmlFile->createElement("ts",$ts);
                    $XMLresource->appendChild($XMLts); 
            }
        }
    }
    $string_value = $xmlFile->saveXML();
    $xmlFile->save(RATES);
}

function CreateISO($ccodes) //Generate countries.xml
{     
    $ISOurl = COUNTRIES_URL;
    $ISOxml = simplexml_load_file($ISOurl) or die("Not Loading");
    
    $xmlFile = new DomDocument("1.0","UTF-8");
    $xmlFile->formatOutput = true;

    $Locations = $xmlFile->createElement("Locations");
    $Locations = $xmlFile->appendChild($Locations);

    foreach ($ISOxml->CcyTbl->CcyNtry as $ISO)
    {
            $ISOcode = $ISO->Ccy;
                  
            foreach ($ccodes as $arraycode)
            {       
                if (strncasecmp($arraycode, $ISOcode, 3) == 0)
                {   
                    $ISOcode = $ISO->Ccy;
                    $ISOFull = $ISO->CcyNm;
                    $ISOloc = $ISO->CtryNm; 
                    
                    $Location = $xmlFile->createElement("Location");
                    $Locations->appendChild($Location);
                    
                    $code = $xmlFile->createElement("code",$ISOcode);
                    $Location->appendChild($code);
                    
                    $code = $xmlFile->createElement("curr",$ISOFull);
                    $Location->appendChild($code);


                    $loc = $xmlFile->createElement("loc",$ISOloc);
                    $Location->appendChild($loc);
                }
            }
    }
    $string_value = $xmlFile->saveXML();
    $xmlFile->save(COUNTRIES);
}

function UpdateRates() //Update Rates.xml when < 12 hours
{
    $Ratexml = simplexml_load_file(RATES) or die("Not Loading"); 
    $RatesURL = simplexml_load_file(RATES_URL) or die("Not Loading");
    
    foreach ($Ratexml->resource as $XMLresource) //Loop local xml
    {
        $XMLcode = $XMLresource->code;
        
        foreach ($RatesURL->resources->resource as $resource)
        {
            $code = substr($resource->field[2],0,-2);
//            foreach ($ccodes as $arraycode)
//            {
//                if(strncasecmp($arraycode,$code, 3) == 0)  
//                {
                    $rate = strval($resource->field[1]);
                    $ts =   (string)$resource->field[3]; 
//                }
//            }
            
            if(strncasecmp($XMLcode,$code, 3) == 0)  
            {
                $XMLresource->rate = $rate;
                $XMLresource->ts = $ts;
                $Ratexml->saveXML(RATES);
            }
        }
    }
    
}

function ErrorHandling($code, $error_hash, $format) //Print Error Messages
{
        $msg = $error_hash[$code];
        
       $string = <<<XML
<?xml version='1.0' encoding="UTF-8"?> 
<conv>
 <error>
     <code>$code</code>
     <msg>$msg</msg>
 </error>
</conv>
XML;
    
     $xml = simplexml_load_string($string);
    
     if ($format == "JSON" || $format == "json")
     {
            header('Content-Type: application/json');
            echo $json = json_encode($xml,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);    
     }
     else
     {
            header('Content-type: text/xml');
            echo $xml->asXML();
     } 
}
?>