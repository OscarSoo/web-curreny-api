<?php

require_once 'config_.php';
require_once 'index_functions.php'; 

$_SERVER['REQUEST_URI']; 
$amount = isset($_GET['amnt']) ? $_GET['amnt'] : null; //Get rid of undefined index error
$inputFrom = isset($_GET['from']) ? $_GET['from'] : null;
$inputTo = isset($_GET['to']) ? $_GET['to'] : null;
$format = isset($_GET['format']) ? $_GET['format'] : null; 


    // #1100 Required parameter is missing when everything is empty
    if (empty($amount)  || empty($inputFrom)  ||empty($inputTo)  || empty($format))
    {
        ErrorHandling(1100, $error_hash, $format);
        exit;
    }
    // #1000 Currency type not recognized error when inputs not match with currency array
    if (!in_array($inputFrom, $ccodes) || !in_array($inputTo, $ccodes)) 
    { 
        ErrorHandling(1000, $error_hash, $format);
        exit;
    }
    // #1200	Parameter not recognized when amount != numeric or  format != alphabets
    if (!is_numeric($amount) || !ctype_alpha($format)) 
    { 
        ErrorHandling(1200, $error_hash, $format);
        exit;
    }
    // #1200	Parameter not recognized when format != xml or json or XML or XML
    if ($format != 'json' && $format != 'xml' && $format != 'JSON' && $format != 'XML') 
    { 
        ErrorHandling(1200, $error_hash, $format);
        exit;
    }
    // #1200	Parameter not recognized when more than 4 parameters
    if (count($_GET) > 4) 
    { 
        ErrorHandling(1200, $error_hash, $format);
        exit;
    }
    // #1300	Currency amount must be a decimal number when amount == numeric
    if (!preg_match('/^[+-]?(\d*\.\d+([eE]?[+-]?\d+)?|\d+[eE][+-]?\d+)$/', $amount))
    { 
        ErrorHandling(1300, $error_hash, $format);
        exit;
    }
    // #1400	Error in service
//    if () 
//    { 
//
//    }

    if (file_exists(COUNTRIES) && file_exists(RATES))  //load both XML files
    {
        $Ratexml = simplexml_load_file(RATES);
        $ISOxml = simplexml_load_file(COUNTRIES);

        foreach ($Ratexml->resource as $resource)
        {
            $RateCode = $resource->code;
            $RateRate = $resource->rate;
            $RateTs   = intval($resource->ts);

            //Check timestamp if it's more than 12 hours 
            if($RateTs  <= time() - (60*60*12) && $RateCode != 'USD')
            {
                UpdateRates();  
            } 
        } 
    } 
    else 
    {
        CreateRates($ccodes); //Run the CreateRates() function in functions.php
        CreateISO($ccodes);   //Run the CreateISO() function in functions.php   
    } 

    XML($ccodes, $inputFrom, $inputTo, $amount, $format); //Run the combine function  
?>